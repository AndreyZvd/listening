$(document).ready(function () {
    let audio = $('#playlist audio');
    audio.each(function () {
        $(this)[0].currentTime = $(this).parent().find('.time').val();
    })

    audio.on('pause', function () {
        let curTime = $(this)[0].currentTime;
        let itemId = $(this).parent().find('.time').attr('data-item-id');
        $.ajax({
            method: "POST",
            url: "/playlist/save-time",
            data: {"_token": $('meta[name="csrf-token"]').attr('content'), time: curTime, itemId: itemId }
        })
            .done(function( msg ) {
                console.log( msg );
            });
    })
});
