<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Playlist extends Model
{
    use HasFactory;

    protected $table = 'playlist';

    public static function getTimeByItemId($itemId)
    {
        return self::where('book_item_id', $itemId)->first()->time;
    }
}
