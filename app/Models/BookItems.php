<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class BookItems extends Model
{
    use HasFactory;

    public const MP3 = 'mp3';

    public static function getAudioTypeByFormat($format): string
    {
        if ($format == self::MP3) {
            return "audio/mpeg";
        }
        return '';
    }
}
