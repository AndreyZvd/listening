<?php

namespace App\Http\Controllers;

use App\Models\Book;
use App\Models\BookItems;
use App\Models\Playlist;
use Illuminate\Http\Request;

class PlaylistController extends Controller
{
    public function index()
    {
        $books = Book::all();
        $bookItems = null;
        if (isset($_GET['bid'])) {
            $bookItems = BookItems::all()->where('book_id', $_GET['bid']);
        }
        return view('playlist.index', compact('books', 'bookItems'));
    }

    public function save()
    {
        if ($_POST['_token']) {
            if ($_POST['itemId']) {
                $playlist = Playlist::where('book_item_id', $_POST['itemId'])->first();
                $playlist->time = $_POST['time'] ?: '0';
                $playlist->save();
            }
        }
    }
}
