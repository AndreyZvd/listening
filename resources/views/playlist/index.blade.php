@extends('layouts.app')

@section('content')
    @guest
        <div class="container">
            <div class="alert alert-danger" role="alert">Необходимо войти или зарегистрироваться</div>
        </div>
    @else
        <div class="container">
            <div class="row">
                <div class="col-md-3">
                    <h4>Книги</h4>
                    <ul>
                        <?php foreach ($books as $book): ?>
                            <li><a href="/playlist/?bid=<?=$book->id?>"><?=$book->book_name?></a></li>
                        <?php endforeach; ?>
                    </ul>
                    <h5>Загрузка новых книг</h5>
                    <form action="" method="post" enctype="multipart/form-data">
                        <div class="form-group">
                            <input type="text" placeholder="Название книги">
                        </div>
                        <div class="form-group">
                            <input type="file" multiple>
                        </div>
                        <button type="submit" class="btn btn-primary">Сохранить</button>
                    </form>
                </div>
                <div class="col-md-9">
                    <div id="playlist">
                        <?php if (isset($_GET['bid']) && $bookItems): ?>
                        <?php foreach ($bookItems as $item): ?>
                            <div class="flex">
                                <div>
                                    <audio id="a-<?=$item->item_name?>" controls>
                                        <source src="/public/upload/playlist/<?=$item->item_name?>.<?=$item->format?>" type="<?=App\Models\BookItems::getAudioTypeByFormat($item->format)?>">
                                    </audio>
                                    <input data-item-id="<?=$item->id?>" type="hidden" class="time" value="<?=\App\Models\Playlist::getTimeByItemId($item->id)?>">
                                </div>
                            </div>
                        <?php endforeach; ?>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
        </div>
    @endguest
@endsection
